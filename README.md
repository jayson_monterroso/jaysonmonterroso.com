# Jaysonmonterroso.com Personal Website

## Deploy instructions

1. After complete the changes, make a `grunt build`

2. If you haven't already, log in to your Heroku account and follow the prompts to create a new SSH public key.   


```
$ heroku login
Clone the repository
```   


3. Use Git to clone jaysonmonterroso's source code to your local machine.



```
$ heroku git:clone -a jaysonmonterroso
$ cd jaysonmonterroso
```


4. Deploy your changes

Make some changes to the code you just cloned deploy them to Heroku using Git.
   

```
$ git add .
$ git commit -am "make it better"
$ git push heroku master 
```
    
    

