'use strict'
angular.module('yeyoApp')
  .controller 'MainCtrl', ($scope, $window, Contact) ->
    # jQuery for page scrolling feature - requires jQuery Easing plugin
    $ ->
      $(".page-scroll a").bind "click", (event) ->
        $anchor = $(this)
        $("html, body").stop().animate
          scrollTop: $($anchor.attr("href")).offset().top
        , 1500, "easeInOutExpo"
        event.preventDefault()
        return
      return
    # Floating label headings for the contact form
    $ ->
      $("body").on("input propertychange", ".floating-label-form-group", (e) ->
        $(this).toggleClass "floating-label-form-group-with-value", !!$(e.target).val()
        return
      ).on("focus", ".floating-label-form-group", ->
        $(this).addClass "floating-label-form-group-with-focus"
        return
      ).on "blur", ".floating-label-form-group", ->
        $(this).removeClass "floating-label-form-group-with-focus"
        return
      return
    # Highlight the top nav as scrolling occurs
    $("body").scrollspy target: ".navbar-fixed-top"

    $scope.tracking =  (track0, track1, track2) ->
      $window.ga('send','event'  , track0, track1, track2);
    $scope.showForm = true;
    $scope.showButton = true;
    $scope.sendEmail = ->
      return if not $scope.email
      $scope.showButton = false;
      Contact.sendEmail(
        emailFrom:$scope.email ,
        name:$scope.name,
        message:$scope.message
      ).then((data) ->
        console.log(data,'data');
        $scope.showForm = false;
      )




