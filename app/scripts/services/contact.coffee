'use strict'
angular.module('yeyoApp')
  .service 'Contact' , ($q, $http) ->
    
    serviceContact = 
      sendEmail: (data) ->
        deferred  = $q.defer()
        emailData =
          key: "Ivoz3qVPZKno7T24uAXOUQ"
          message:
            html: "<p>A user click in the contact form the data here: </p><p>Message from" + data.name + "</p><br/></br>"+ data.message
            text: ""
            subject: "Contact Form - jaysonmonterroso.com"
            from_email: data.emailFrom
            from_name: data.name
            to: [
              email: "jayson.monterroso@gmail.com"
              name: "Jayson Monterroso"
              type: "to"
            ]
            headers:
              "Reply-To": data.emailFrom
        $http(
          method:'POST'
          ,url: 'https://mandrillapp.com/api/1.0/messages/send.json'
          ,data: emailData
          ).success( (data, status, headers, config)->
            deferred.resolve(data);
          )
        deferred.promise
    # AngularJS will instantiate a singleton by calling "new" on this function
