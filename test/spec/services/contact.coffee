'use strict'

describe 'Service: Contact', ->

  # load the service's module
  beforeEach module 'yeyoApp'

  # instantiate service
  Contact = {}
  beforeEach inject (_Contact_) ->
    Contact = _Contact_

  it 'should do something', ->
    expect(!!Contact).toBe true
